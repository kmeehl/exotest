defmodule Exotest do
  use Application
  use Elixometer

  def start(_type, _args) do
    update_counter("Exotest.start.count", 1)

    IO.puts("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Hello World")
  end

end
