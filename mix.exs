defmodule Exotest.Mixfile do
  use Mix.Project

  def project do
    [
      app: :exotest,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
{:elixometer, "~> 1.2"},
{:exometer_core, "~> 1.5"},
{:exometer_influxdb, "~> 0.5.7"},
{:setup, github: "uwiger/setup", manager: :rebar, override: true},
{:pobox, "~> 1.0"},

    ]
  end
end
