# Exotest

##A simple app to show an error returned by Exometer using exometer_influxdb

In iex, run:
```
iex(2)> Exotest.start(nil, nil)
```
